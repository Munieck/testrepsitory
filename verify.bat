@echo off

for %%a in (.) do set currentfolder=%%~na

cd "..\SpaceEngineers\"

".\SEWorkshopTool.exe" --upload --dry-run --compile --mods "%appdata%\SpaceEngineers\Mods\%currentfolder%" --exclude .csproj .sln .user .gitignore .bat

echo verify finished

timeout 10